sample_rate = 22050
num_mels = 80
num_fft = 1024
hop_length = 256
min_freq = 125
max_freq = 7.6 * 1e3

# Encoder
embedding_dim = 512
encoder_conv_filters = 512
encoder_conv_len = 5
encoder_num_conv = 3
encoder_lstm_hidden = 256

# Attention
attn_hidden = 128
attn_conv_filters = 32
attn_conv_len = 31
attn_window_len = 5

# Decoder
decoder_num_pre = 2
decoder_pre_hidden = 256
decoder_num_lstm = 2
decoder_lstm_hidden = 1024
decoder_num_conv = 5
decoder_conv_filters = 512
decoder_conv_len = 5

# Regularizers
p_dropout = 0.5
p_zoneout = 0.1

# Optimizer
lr_initial = 1e-3
lr_final = 1e-5
lr_decay_iterations = 50000
lr_decay_rate = 1 - 1e-4
weight_decay = 1e-6

# Training
train_files = -1
val_split = 0.01
batch_size = 16
use_gpu = True

# Curriculum Learning
expected_convergence_epochs = 250
min_teacher_prob = 1e-6#7e-4
similarity_mode = 1e-3