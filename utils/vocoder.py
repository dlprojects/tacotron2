import numpy as np
import librosa

frame_length_ms=50
frame_shift_ms=12.5
num_freq=1024
sample_rate=22050

def _stft_parameters():
  n_fft = (num_freq - 1) * 2
  hop_length = int(frame_shift_ms / 1000 * sample_rate)
  win_length = int(frame_length_ms / 1000 * sample_rate)
  return n_fft, hop_length, win_length

def _griffin_lim(S, iters=60):
  '''librosa implementation of Griffin-Lim
  Based on https://github.com/librosa/librosa/issues/434
  '''
  angles = np.exp(2j * np.pi * np.random.rand(*S.shape))
  S_complex = np.abs(S).astype(np.complex)
  y = _istft(S_complex * angles)
  for i in range(iters):
    angles = np.exp(1j * np.angle(_stft(y)))
    y = _istft(S_complex * angles)
  return y

  def _stft(y):
    n_fft, hop_length, win_length = _stft_parameters()
    return librosa.stft(y=y, n_fft=n_fft, hop_length=hop_length, win_length=win_length)


def _istft(y):
    _, hop_length, win_length = _stft_parameters()
    return librosa.istft(y, hop_length=hop_length, win_length=win_length)


